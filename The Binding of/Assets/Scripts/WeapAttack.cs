using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeapAttack : MonoBehaviour
{
    public GameObject weap;
    private int weapDmg = -10;
    public Health iA;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            iA = other.gameObject.GetComponent<Health>();

            iA.UpdateLife(weapDmg);
        }
    }
}

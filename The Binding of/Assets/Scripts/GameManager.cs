using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//GameManager
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    [SerializeField]private List<GameObject> salas;
    [SerializeField]private Text finalTxt;
    [SerializeField]private GameObject exitBtn;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        EventManager.StartGame();
    }

    void Update()
    {
        if (Detect())
        {
            Time.timeScale = 0;
            finalTxt.text = "Win";
            exitBtn.SetActive(true);
        }
    }

    private bool Detect()
    {
        foreach (GameObject ia in salas)       //detectar salas completas
        {
            if (ia != null)
            {
                return false;
            }
        }
        return true;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}

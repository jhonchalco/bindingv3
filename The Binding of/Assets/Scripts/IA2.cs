using UnityEngine;
using UnityEngine.AI;

public class IA2 : MonoBehaviour
{
    private NavMeshAgent agent;
    private Transform player;
    public LayerMask isGround, isPlayer;
    public GameObject bullet;
    public Transform bulletParent;
    private Animator anim;

    public Vector3 walkPoint;
    private bool walkPointBool;
    public float walkPointRange;

    public float atkSpeed;
    private bool isAtk;

    public float range, atkRange;
    private bool isNear, isAtkRange;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player").transform;
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        isNear = Physics.CheckSphere(transform.position, range, isPlayer);
        isAtkRange = Physics.CheckSphere(transform.position, atkRange, isPlayer);

        if (!isNear && !isAtkRange) Route();        //estados de IA
        if (isNear && !isAtkRange) Follow();
        if (isNear && isAtkRange) Attack();
    }

    private void Route()
    {
        if (!walkPointBool) GenerateRoute();
        else agent.SetDestination(walkPoint);       //seguir ruta random

        anim.SetBool("isWalk", true);           //animaciones
        anim.SetBool("isAtk", false);

        Vector3 distanceWalk = transform.position - walkPoint;

        if (distanceWalk.magnitude < 1f) walkPointBool = false; 
    }

    private void GenerateRoute()
    {
        float rndZ = Random.Range(-walkPointRange, walkPointRange);
        float rndX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + rndX, +transform.position.y, transform.position.z + rndZ);       //generar ruta random

        if (Physics.Raycast(walkPoint, -transform.up, 2f, isGround)) walkPointBool = true;
    }

    private void Follow()
    {
        agent.SetDestination(player.position);      //seguir al player
        anim.SetBool("isWalk", true);
        anim.SetBool("isAtk", false);
    }

    private void Attack()
    {
        agent.SetDestination(transform.position);
        anim.SetBool("isWalk", false);

        transform.LookAt(player);

        if (!isAtk)
        {
            isAtk = true;
            Invoke(nameof(ResetAtk), atkSpeed);
        }
    }

    private void ResetAtk()
    {
        anim.SetBool("isAtk", true);
        Rigidbody rb = Instantiate(bullet, bulletParent.position, Quaternion.identity).GetComponent<Rigidbody>();

        if (Vector3.Distance(player.transform.position, transform.position) < 7.5f)         //lanzar flecha dependiendo de la distancia con el jugador
        {
            rb.AddForce(transform.forward * 7f, ForceMode.Impulse);
            rb.AddForce(transform.up * 4f, ForceMode.Impulse);
        }
        else if (Vector3.Distance(player.transform.position, transform.position) < 11f)
        {
            rb.AddForce(transform.forward * 9f, ForceMode.Impulse);
            rb.AddForce(transform.up * 5f, ForceMode.Impulse);
        }
        else
        {
            rb.AddForce(transform.forward * 10f, ForceMode.Impulse);
            rb.AddForce(transform.up * 6f, ForceMode.Impulse);
        }
        isAtk = false;
    }
}

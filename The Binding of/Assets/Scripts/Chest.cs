using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest : MonoBehaviour
{
    [SerializeField]
    private GameObject weapAxe;
    [SerializeField]
    private GameObject weapSword;
    [SerializeField]
    private GameObject weapDual;
    public bool axe;
    public bool sword;
    public bool dualhand;
    private Animator animator;
    public GameObject weaponSelector;
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("OpenChest", false);
    }

    public void OpenChest()
    {
        animator.SetBool("OpenChest", true);        //Abrir el cofre, inicia la animacion
        weaponSelector.SetActive(true);     
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            OpenChest();
        }
    }

    public void SelectWeapon(int wp)
    {
        switch (wp) //cambiar armas
        {
            case 1:
                //Cambia arma 
                axe = true;
                weapAxe.SetActive(true);
                weapDual.SetActive(false);
                weapSword.SetActive(false);
                break;
            case 2:
                //Cambia a espada
                sword = true;
                weapSword.SetActive(true);
                weapAxe.SetActive(false);
                weapDual.SetActive(false);
                
                break;
            case 3:
                dualhand = true;
                weapDual.SetActive(true);
                weapAxe.SetActive(false);
                weapSword.SetActive(false);
                //Spear
                break;
        }
    }
}

using System.Collections.Generic;
using UnityEngine;

public class Salas : MonoBehaviour
{
    public List<GameObject> IA;
    public GameObject chest;
    public List<MoveDoors> door;

    void Update()
    {
        if(Detect())
        {
            if (chest != null) chest.SetActive(true);      //Cuando las Ias mueran, dar la sala por finalizada
            if(door != null)
            {
                foreach(MoveDoors mdoor in door)
                {
                    mdoor.StartOpen();
                }
            }
            Destroy(transform.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            foreach (GameObject ia in IA)
            {
                ia.SetActive(true);         //activar Ias al entrar el player en la sala
            }
        }
    }

    private bool Detect()
    {
        foreach (GameObject ia in IA)       //detectar Ias
        {
            if(ia != null)
            {
                return false;
            }
        }
        return true;
    }
}

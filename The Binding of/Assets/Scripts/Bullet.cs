using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Health player;
    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<Health>();
    }
    private void OnTriggerEnter(Collider other)     //Colision para restar vida al player
    {
        if (other.CompareTag("Player"))
        {
            player.UpdateLife(-5);
            Destroy(transform.gameObject);
        }
        else
        {
            Destroy(transform.gameObject);
        }
    }
}

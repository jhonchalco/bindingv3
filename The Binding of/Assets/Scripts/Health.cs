using MiscUtil.Collections.Extensions;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Health : MonoBehaviour
{
    public TextMeshPro life;
    private int health = 0;
    public int initialHealth;

    private void Start()
    {
        UpdateLife(initialHealth);      //resgistar la vida
    }

    public void UpdateLife(int n)
    {
        health = health + n;        //asignar la nueva cantidad para la vida
        Debug.Log(health);

        if(health <= 0)             //detectar si el elemento a perdido toda su vida
        {
            if (transform.CompareTag("Player"))     
            {
                print("Haz muerto");
            }
            else Destroy(transform.gameObject);
        }
        life.text = health.ToString();
    }
}

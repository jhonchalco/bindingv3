using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    [Header("Movement Settings")]
    [SerializeField]
    public float speedNormal = 5;
    [SerializeField]
    private float speedOnAirDecrease = 0;
    [SerializeField]
    private float speedTimeFreez = 40f;
    [SerializeField]
    public bool isStoped = false;
    [Header("Jump Sttings")]
    [SerializeField]
    private float gravity = -30f;

    public GameObject swordWeap;
    public GameObject axeWeap;
    public GameObject maceWeap;

    private bool movement;

    private Vector2 directionMove;
    private Vector3 velocity;

    private bool speedTimeStop = false;
    private Camera mainCamera;
    [HideInInspector]
    public CharacterController controller;
    [SerializeField] private Transform player;

    private Animator animator;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (directionMove == Vector2.zero)
        {
            animator.SetFloat("movement", 0);
            Debug.Log(directionMove);
        }
        else
        {
            animator.SetFloat("movement", 1);
            Debug.Log("No Entro");
        }
        if (speedTimeStop == false)
        {
            PlayerMovement(new Vector3(directionMove.x, 0, directionMove.y), speedNormal);
        }
        if (speedTimeStop == true)
        {
            PlayerMovement(new Vector3(directionMove.x, 0, directionMove.y), speedTimeFreez);
        }
    }
    private void OnMove(InputValue value)
    {

        directionMove = value.Get<Vector2>();
        if (directionMove.x > 0)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        else if (directionMove.x < 0)
        {
            transform.eulerAngles = new Vector3(0, -90, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        else if (directionMove.y < 0)
        {
            transform.eulerAngles = new Vector3(0, -180, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        else if (directionMove.y > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }

    }
    private void OnRoll(InputValue value)
    {
        StartCoroutine("Rolling");

    }

    IEnumerator Rolling()
    {
        controller.height = 1;
        animator.SetBool("roll", true);
        yield return new WaitForSeconds(0.8f);
        controller.height = 2;
        animator.SetBool("roll", false);
    }

    public void PlayerMovement(Vector3 direction, float speed)
    {

        if (controller.isGrounded)
        {
            velocity = direction;
            velocity *= speed;

        }
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
        if (controller.isGrounded == false)
        {
            if (controller.collisionFlags == CollisionFlags.Above)
            {
                velocity.y += gravity * Time.deltaTime;
            }
            Vector3 velocityAir = new Vector3(directionMove.x, 0, directionMove.y);
            velocityAir *= speed - speedOnAirDecrease;
            controller.Move(velocityAir * Time.deltaTime);

        }
    }
    private void OnAttack()
    {
        StartCoroutine("AttackSword");
    }
    IEnumerator AttackSword()
    {
        animator.SetBool("attack1", true);
        yield return new WaitForSeconds(1f);
        animator.SetBool("attack1", false);
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IA3 : MonoBehaviour
{
    private NavMeshAgent agent;
    private Transform player;
    private Health playerH;
    public LayerMask isGround, isPlayer;
    private Animator anim;

    public Vector3 walkPoint;
    private bool walkPointBool;
    public float walkPointRange;

    public float atkSpeed;
    private bool isAtk;

    public float range, atkRange;
    private bool isNear, isAtkRange;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player").transform;
        playerH = GameObject.Find("Player").GetComponent<Health>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        isNear = Physics.CheckSphere(transform.position, range, isPlayer);
        isAtkRange = Physics.CheckSphere(transform.position, atkRange, isPlayer);

        if (!isNear && !isAtkRange) Route();        //estados de IA
        if (isNear && !isAtkRange) Follow();
        if (isNear && isAtkRange) Attack();
    }

    private void Route()
    {
        if (!walkPointBool) GenerateRoute();
        else agent.SetDestination(walkPoint);       //mover la IA por la ruta aleatoria

        anim.SetFloat("speedh", agent.velocity.z);
        anim.SetFloat("speedv", agent.velocity.x);      //animacion de la IA

        Vector3 distanceWalk = transform.position - walkPoint;

        if (distanceWalk.magnitude < 1f) walkPointBool = false;
    }

    private void GenerateRoute()
    {
        float rndZ = Random.Range(-walkPointRange, walkPointRange);
        float rndX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + rndX, +transform.position.y, transform.position.z + rndZ);       //Generar ruta aleatoria a seguir

        if (Physics.Raycast(walkPoint, -transform.up, 2f, isGround)) walkPointBool = true;
    }

    private void Follow()
    {
        agent.SetDestination(player.position);      //seguir al jugador
    }

    private void Attack()
    {
        agent.SetDestination(transform.position);       //parar a la IA para atacar

        Vector3 newPos = new Vector3(player.position.x, transform.position.y, player.position.z);

        transform.LookAt(newPos);       //mirar al player

        if (!isAtk)
        {
            anim.SetBool("Attack1h1", true);
            isAtk = true;
            if (isAtkRange)
            {
                playerH.UpdateLife(-10);
            }
            Invoke(nameof(ResetAtk), atkSpeed);     
        }
    }

    private void ResetAtk()
    {
        isAtk = false;      
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ODoor : MonoBehaviour
{
    public MoveDoors door;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {   
            door.StartOpen();        //abir puerta al player acercarse
            Destroy(transform.gameObject);
        }
    }
}

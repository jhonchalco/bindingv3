using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDoors : MonoBehaviour
{
    private GameObject door;
    private Vector3 pos2;
    public bool open, stop;
    private void Start()
    {
        door = transform.gameObject;
        open = false;
        stop = true;
        pos2 = door.transform.position;
        pos2.y += 4f;
    }

    private void Update()
    {
        if (!stop)
        {
            if (open) OpenDoor();
        }
    }

    public void StartOpen()
    {
        stop = false;
        open = true;
        StartCoroutine(StopDoor());
    }

    private void OpenDoor()
    {
        door.transform.position = Vector3.MoveTowards(transform.position, pos2, 2 *Time.deltaTime);
    }

    IEnumerator StopDoor()
    {
        yield return new WaitForSeconds(3f);
        stop = true;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IA1 : MonoBehaviour
{
    public int IADmg;
    private GameObject player;
    private NavMeshAgent agent;
    public float detectDistance, atkDistance;
    public float velocity;
    private Health healthP;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player");
        healthP = GameObject.Find("Player").GetComponent<Health>();
    }

    void Update()
    {
        if (Vector3.Distance(player.transform.position, transform.position) < detectDistance)
        {
            agent.SetDestination(player.transform.position);        //seguir al player
            transform.LookAt(player.transform);
            agent.speed = velocity;
        }
        else
        {
            agent.speed = 0;
        }

        if (Vector3.Distance(player.transform.position, transform.position) < atkDistance)      //atacar y destruir IA
        {
            healthP.UpdateLife(IADmg);
            Destroy(transform.gameObject);
            print("atk");
        }
    }
}
